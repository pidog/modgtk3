

## modGtk3 for Xojo v1.0.20

For current version:
   https://bitbucket.org/pidog/modgtk3/src/master/

Modifies CSS and adjusts control sizes in Xojo apps deployed to Linux platforms to create a more native interface.

see https://forum.xojo.com/48126-gtk3-theming-modgtk3-resolves-layout-corruption-under-all-linux/last

Includes code from J�rg Otter, Jim McKay, and others (please add here).

Thanks to Tim Jones for starting the thread.

**Getting started**

Add the modGtk3 module to your Xojo app.

In the open event of the app object, add the following lines-

```
    modGTK3.initGtkEntryFix  // adjusts the char-widths property of GtkEntry to be 0
    modGTK3.initGtkWidgetHeightFix // adjusts all controls to be at least their minimum height
    modGTK3.InitGlobalGTK3Style  // various CSS tweaks to override theme CSS
```
modGtk3.kGlobalGTK3CSS contains the default CSS override.

Please feel free to expirement with modifications for specific platform combinations.

## To include conditionals in the CSS:

Sections within '#IF' and '#END IF' will be removed if the conditions are not  met

can be  XOJO<|>[RBVersion]  OS=[OS Name] RELEASE<|(!)=|>[Release number] DESKTOP(!)=[Desktop] SESSION(!)=[Session] or any combination

parsing is LTR with no nested logic (no parens)

example:

\#IF XOJO<2018 AND OS=LinuxMint  OR RELEASE>17   <--this will always pass if Release is above 17!

---a bunch of conditional CSS---

\#END IF


Comments are also allowed

\# This is a comment

---
