#tag Class
Protected Class App
Inherits Application
	#tag Event
		Sub Open()
		  modGTK3.initGtkEntryFix
		  modGTK3.initGtkWidgetHeightFix
		  modGTK3.InitGlobalGTK3Style
		  
		  CSS_Editor.Show
		  Gtk_Style_Viewer.Show
		  Gtk_Style_Viewer.top=CSS_Editor.top+CSS_Editor.Height+60
		  Gtk_Style_Viewer.Left=CSS_Editor.Left
		  
		  Window1.Show
		  window1.Left=CSS_Editor.left+CSS_Editor.Width+10
		  
		  PropertiesViewer.Show
		  PropertiesViewer.Left=Gtk_Style_Viewer.Left+Gtk_Style_Viewer.Width+10
		  PropertiesViewer.top=window1.top+window1.Height+60
		End Sub
	#tag EndEvent


	#tag MenuHandler
		Function EditStopSampling() As Boolean Handles EditStopSampling.Action
			if Window1.SampleTimer.Mode=2 then
			Window1.SampleTimer.mode=0
			else
			Window1.SampleTimer.mode=2
			end if
			Return True
			
		End Function
	#tag EndMenuHandler


	#tag Constant, Name = kEditClear, Type = String, Dynamic = False, Default = \"&Delete", Scope = Public
		#Tag Instance, Platform = Windows, Language = Default, Definition  = \"&Delete"
		#Tag Instance, Platform = Linux, Language = Default, Definition  = \"&Delete"
	#tag EndConstant

	#tag Constant, Name = kFileQuit, Type = String, Dynamic = False, Default = \"&Quit", Scope = Public
		#Tag Instance, Platform = Windows, Language = Default, Definition  = \"E&xit"
	#tag EndConstant

	#tag Constant, Name = kFileQuitShortcut, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Mac OS, Language = Default, Definition  = \"Cmd+Q"
		#Tag Instance, Platform = Linux, Language = Default, Definition  = \"Ctrl+Q"
	#tag EndConstant


	#tag ViewBehavior
	#tag EndViewBehavior
End Class
#tag EndClass
